import s3neo.queries
import json
import time
import click
import neutils.with_time as nu
from s3neo.queries._queries import break_date_int, execute_athena_query, get_athena_client

@click.command()
@click.option('--user_id', type=int)
@click.option('--start_date', type=int)
@click.option('--end_date', type=int)
@click.option('--region', default='', type=str, multiple=False, help='')
@click.option('--database', default='', type=str, multiple=False, help='')
@click.option('--table', default='', type=str, multiple=False, help='')

def timestamp_to_dateint(timestamp, timezone):
    current_time = time.time()
    if timestamp/current_time > 100:
        date = nu.time.timestamp_to_local_time_str(timestamp/1000, timezone, "yyyyMMdd")
    else:
        date = nu.time.timestamp_to_local_time_str(timestamp, timezone, "yyyyMMdd")
    return int(date)

def user_raw_data_by_channel_and_dateint_wo_headers(user_id, start_date, end_date, region_name, database, table):
    """Returns the Raw Data of a specific channel by DateInt

    Arguments
    ---------
    user_id : int
        The user_id.
    start_date : int
        The date to start taking the data from.
    end_date : int
        The date to finish taking the data from.
    region_name : str
        The region of the amazon environment to connect to.
    database : str
        The database to retrieve the query results from.
    table : str
        The table to retrieve the query results from.
    channel_names : list
        A list of channel names to be extracted.
        Channels: visits, geolocations, geofencing, minutes, awareness, transit, routers, steps, bluetooth, wifi, bluetooth_gatt, device_states, idles
        Sensors: s_pressures, s_screen_states, s_temperatures, s_power_states, s_gps_satellites, s_gps_altitudes, connected_bts, test_events
        Other: ratatouille, placer

    Returns
    -------
    dict
        A dictionary where the key is the raw data collection name and the value is a list of raw data samples,
        each sample is a dictionary with data on the sample.
    """

    results_bucket_name = 'neura-qacript-query-result-raw-data'

    channel_names = ['visits', 'geolocations', 'geofencing', 'minutes', 'awareness', 'transit', 'routers', 'steps',
                     'bluetooth', 'wifi', 'bluetooth_gatt',
                     'device_states', 'device_orientation', 'screen_brightness', 'idles', 's_pressures',
                     's_screen_states', 's_temperatures', 's_power_states',
                     's_gps_satellites', 's_gps_altitudes', 'connected_bts', 'test_events', 'ratatouille', 'placer']

    date_int = start_date

    user_channel_data = {}

    while date_int <= end_date:
        print('Collecting raw data for user {} date {}'.format(user_id, date_int))

        athena_client = get_athena_client(region_name)
        year, month, day = break_date_int(date_int)

        # create the query command
        query = 'SELECT * FROM "{}"."{}" WHERE user_id={} AND year={} AND month={} AND day={};'.format(database, table,
                                                                                                       user_id, year,
                                                                                                       month, day)

        results = execute_athena_query(query, athena_client, results_bucket_name, True)
        headers = results['ResultSet']['Rows'][0]['Data']

        for entry in results['ResultSet']['Rows'][1:]:
            index = 0
            for i in entry['Data']:
                if headers[index]['VarCharValue'] == 'value':

                    data = json.loads(entry['Data'][index]['VarCharValue'])

                    # Get data headers
                    if 'timezone' in data['meta-data'].keys():
                        user_timezone = data['meta-data']['timezone'][0]
                    else:
                        user_timezone = 'None'

                    metadata = {'user_timezone': user_timezone}

                    # Get the desired data

                    if 'channels' in data.keys():
                        for channel, channel_data in data['channels']['channel'].items():
                            if channel in channel_names:
                                if channel not in user_channel_data.keys():
                                    user_channel_data[channel] = []

                                if channel == 'minutes':
                                    for item in channel_data:
                                        if item is not None:
                                          user_channel_data[channel].append(item)

                                elif channel == 'visits':
                                    for item in channel_data:
                                        if item is not None:
                                            user_channel_data[channel].append(item)

                                else:
                                    for item in channel_data:
                                        if item is not None:
                                            user_channel_data[channel].append(item)

                    if 'sensors' in data.keys():
                        if data['sensors']['collection'] in channel_names:
                            if data['sensors']['collection'] not in user_channel_data.keys():
                                user_channel_data[data['sensors']['collection']] = []
                            for item in data['sensors']['items']:
                                user_channel_data[data['sensors']['collection']].append(item)

                    if 'bluetooth' in data.keys():
                        if 'bluetooth' in channel_names:
                            if 'bluetooth' not in user_channel_data.keys():
                                user_channel_data['bluetooth'] = []
                            if 'devices' in data['bluetooth'].keys():
                                for device in data['bluetooth']['devices']:
                                    user_channel_data['bluetooth'].append(device)
                            else:
                                print('bluetooth raw data in an unrecognized format')

                    if 'wifi' in data.keys():
                        if 'wifi' in channel_names:
                            if 'wifi' not in user_channel_data.keys():
                                user_channel_data['wifi'] = []
                            for device in data['wifi']['devices']:
                                user_channel_data['wifi'].append(device)

                    if 'device_states' in data.keys():
                        if 'device_states' in channel_names:
                            if 'device_states' not in user_channel_data.keys():
                                user_channel_data['device_states'] = []
                            for item in data['device_states']['items']:
                                user_channel_data['device_states'].append(item)

                    if 'ratatouille' in data.keys():
                        if 'ratatouille' in channel_names:
                            if 'ratatouille' not in user_channel_data.keys():
                                user_channel_data['ratatouille'] = []
                            for rat_item in data['ratatouille']['items']:
                                user_channel_data['ratatouille'].append(rat_item)

                    if 'bluetooth_gatt' in data.keys():
                        if 'bluetooth_gatt' in channel_names:
                            if 'bluetooth_gatt' not in user_channel_data.keys():
                                user_channel_data['bluetooth_gatt'] = []
                            user_channel_data['bluetooth_gatt'].append(data['bluetooth_gatt'])

                    if 'placerTriggers' in data.keys():
                        if 'placer' in channel_names:
                            if 'placer' not in user_channel_data.keys():
                                user_channel_data['placer'] = []
                            data['placerTriggers']['data'].update(metadata)
                            user_channel_data['placer'].append(data['placerTriggers']['data'])

                    if 'channels' not in data.keys() \
                            and 'sensors' not in data.keys() \
                            and 'bluetooth' not in data.keys() \
                            and 'wifi' not in data.keys() \
                            and 'placerTriggers' not in data.keys() \
                            and 'device_states' not in data.keys() \
                            and 'ratatouille' not in data.keys() \
                            and 'bluetooth_gatt' not in data.keys():
                        print('There are more channels that are not displayed in the list ... please investigate')

                index += 1

        date_int = nu.time.shift_dateint(date_int, 1)

    return user_channel_data


def main(user_id,start_date,end_date,region,database,table):
    """
    in this part the import of the data is executed, in this stage data is imported from staging environment
    :return: 
    """
    """
    this is the reference data of 4.0 ios version taken from confluence
    """
    data_4v ={
        "routers":
            {
          "data_collection_timestamp": 1525134218,
          "mac": "d0:d4:12:6d:22:82",
          "syncSource": "GeofenceEventDetected",
          "timezone": "Asia/Jerusalem",
          "connected": "true",
          "application": "il.co.bezeq.publicwifi",
          "timestamp": 1525134218,
          "ip": "10.100.102.1",
          "name": "Shay"
        },
        "geofencing":
            {
            "speed": -1,
            "bearing": -1,
            "region_center_lat": 32.0332500061052,
            "data_collection_timestamp": 1525167446,
            "horisontal_accuracy": 65,
            "timestamp": 1525167446,
            "vertical_accuracy": 10,
            "region_center_lon": 34.763423084303,
            "is_entrance": 0,
            "user_location_lat": 34.76640085684321,
            "user_distance_from_center": 283.38392594910704,
            "related_node_id": "nj8cedfapimq",
            "operating_system": "iOS",
            "user_location_lon": 34.76640085684321
            },
         "visits":
                {
                    "sample_timestamp": 1525167341,
                    "lat": 32.031094520471896,
                    "arrival_date": 1525166974,
                    "lon": 34.765601815585946,
                    "horisontal_accuracy": 132.94356779027504,
                    "data_collection_timestamp": 1525167341
                },
         "steps":
                {
                    "steps": 14,
                    "dataCollectionTimestamp": 1525167145,
                    "timestamp": 1525167000
                },
         "geolocations":
                {
                    "speed": -1,
                    "bearing": -1,
                    "provider": "location",
                    "lon": 34.76494034130713,
                    "horisontal_accuracy": 65,
                    "battery": 0.05999999865889549,
                    "timestamp": 1525167143,
                    "syncSource": "EventDetectedLocally",
                    "vertical_accuracy": 10,
                    "application": "il.co.bezeq.publicwifi",
                    "data_collection_timestamp": 1525167144,
                    "lat": 32.03091009198747,
                    "timezone": "Asia/Jerusalem",
                    "operating_system": "iOS"
                },
            "minutes":
                {
                    "source": "m7",
                    "ios_confidence": "high",
                    "start": 1525167142,
                    "sub_activities": [
                        {
                            "name": "walking",
                            "activity_type": 7,
                            "ios_confidence": "high"
                        }
                                        ],
                    "fetch_timestamp": 1525167145,
                    "activity_type": 2,
                    "syncSource": "EventDetectedLocally",
                    "data_collection_timestamp": 1525167145,
                    "name": "on_foot"
                }
    }

    # tables of athena from which data is taken

    athena_client = s3neo.queries.get_athena_client(region)
    s3neo.queries.execute_load_partitions(athena_client, table, database, 'neura-qacript-query-result-raw-data')

    #data importing
    tested =list([])
    data = user_raw_data_by_channel_and_dateint_wo_headers(user_id, start_date, end_date, region, database, table)

    print("\ntesting for new features in version 5.0 in comparison ti version 4 \n")

    for channels,entries in data.items():

        print("testing channel {}".format(channels))

        for key, val in data_4v.items():
            if key == channels:
                for sub_key,sub_val in entries[0].items():
                    if sub_key not in val.keys():
                        print("{} ".format(sub_key))
        print("")

    print("\ntesting for missing in version 5.0 in comparison ti version 4\n")

    for key, val in data_4v.items():
        print("testing channel {}".format(key))

        for channels,entries in data.items():
            if key == channels and channels not in tested:
                for sub_key,sub_val in val.items():
                    if sub_key not in entries[0].keys():
                        print("{} ".format(sub_key))
                tested.append(channels)
        print("")


if __name__ == '__main__':
    main()